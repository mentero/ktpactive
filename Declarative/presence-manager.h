/*
    Copyright (C) 2012  Artur Dębski <xmentero@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef PRESENCE_MANAGER_H
#define PRESENCE_MANAGER_H

#include <TelepathyQt/AccountManager>
#include <TelepathyQt/ContactManager>
#include <TelepathyQt/Presence>

#include <KTp/presence.h>

#include <TelepathyQt/Types>

namespace KTp {
    class GlobalPresence;
}

namespace Tp {
    class PendingOperation;
}

/** Exposes general contact list stuff to QML*/
class PresenceManager : public QObject
{
    Q_OBJECT

    Q_ENUMS(PresenceType)

    Q_PROPERTY(KTp::Presence presence
           READ presence
           NOTIFY presenceChanged)

    Q_PROPERTY(QString message
           READ message
           //WRITE setPresence
           NOTIFY presenceChanged)

    Q_PROPERTY(QString iconName 
            READ iconName
            NOTIFY presenceChanged)

  public:
    enum PresenceType {
        Online = 0,
        Busy,
        Away,
        ExtendedAway,
        Hidden,
        Offline
    };

    PresenceManager(QObject *parent=0);
    ~PresenceManager();
    void init();

    KTp::Presence presence() const;
    QString message() const;
    QString iconName() const;

  Q_SIGNALS:
    void presenceChanged();

  public Q_SLOTS:
    Q_INVOKABLE void setPresence(PresenceType presence);

  private Q_SLOTS:
    void onAccountsChanged();
    void onAccountManagerReady(Tp::PendingOperation *op);
    void onGenericOperationFinished(Tp::PendingOperation *op);

    void onPresenceChanged(KTp::Presence presence);
    void onConnectionStatusChanged(Tp::ConnectionStatus connectionStatus);
  private:
    /** the number of instances which have tried to register the DBus service*/
    static int s_instanceCount;

    Tp::AccountManagerPtr m_accountManager;
    KTp::GlobalPresence  *m_globalPresence;
};

#endif
