/*
    Copyright (C) 2012  Artur Dębski <xmentero@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "presence-manager.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>

#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/PendingContacts>
#include <TelepathyQt/PendingReady>
#include <TelepathyQt/PendingChannelRequest>
#include <TelepathyQt/Account>

#include <KDebug>
#include <KTp/global-presence.h>

int PresenceManager::s_instanceCount = 0;

PresenceManager::PresenceManager(QObject *parent)
    : QObject(parent),
    m_globalPresence(new KTp::GlobalPresence(this))
{
    init();
    s_instanceCount ++;

    connect(m_globalPresence, SIGNAL(currentPresenceChanged(KTp::Presence)), SLOT(onPresenceChanged(KTp::Presence)));
    onPresenceChanged(m_globalPresence->currentPresence());

    connect(m_globalPresence, SIGNAL(connectionStatusChanged(Tp::ConnectionStatus)), SLOT(onConnectionStatusChanged(Tp::ConnectionStatus)));
    onConnectionStatusChanged(m_globalPresence->connectionStatus());
}

PresenceManager::~PresenceManager()
{
    s_instanceCount--;
    // if (s_instanceCount == 0) {
    //     QDBusConnection::sessionBus().unregisterService("org.kde.Telepathy.PresenceManager");
    // }
}

void PresenceManager::init()
{
    // QDBusConnection::sessionBus().registerService("org.kde.Telepathy.PresenceManager");

    Tp::registerTypes();

    // setup the telepathy account manager from where I'll retrieve info on accounts and contacts
    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                       Tp::Features() << Tp::Account::FeatureCore
                                                                       << Tp::Account::FeatureProtocolInfo
                                                                       << Tp::Account::FeatureProfile);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
                                                                               Tp::Features() << Tp::Connection::FeatureCore
                                                                               << Tp::Connection::FeatureRoster
                                                                               << Tp::Connection::FeatureSelfContact);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());

    m_accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
                                                  accountFactory,
                                                  connectionFactory,
                                                  channelFactory);

    connect(m_accountManager.data(), SIGNAL(newAccount(Tp::AccountPtr)), SLOT(onAccountsChanged()));
    connect(m_accountManager->becomeReady(), SIGNAL(finished(Tp::PendingOperation*)), this, SLOT(onAccountManagerReady(Tp::PendingOperation*)));
}


void PresenceManager::onAccountManagerReady(Tp::PendingOperation *op)
{
    Q_UNUSED(op);
    m_globalPresence->setAccountManager(m_accountManager);
}

void PresenceManager::onAccountsChanged() {

}

KTp::Presence PresenceManager::presence() const
{
    return m_globalPresence->currentPresence();
}

QString PresenceManager::iconName() const
{
  return m_globalPresence->currentPresence().iconName();
}

QString PresenceManager::message() const
{
  return m_globalPresence->currentPresence().displayString();
}

void PresenceManager::setPresence(PresenceManager::PresenceType presence)
{
    KTp::Presence p;
    switch(presence) {
        case Online:
            p = KTp::Presence(Tp::Presence::available());
            break;
        case Busy:
            p = KTp::Presence(Tp::Presence::busy());
            break;
        case Away:
            p = KTp::Presence(Tp::Presence::away());
            break;
        case ExtendedAway:
            p = KTp::Presence(Tp::Presence::xa());
            break;
        case Hidden:
            p = KTp::Presence(Tp::Presence::hidden());
            break;
        case Offline:
            p = KTp::Presence(Tp::Presence::offline());
            break;
        default:
            //This should never happen
            Q_ASSERT(false);
            return;
    }
    p.setStatus(p.type(), p.status(), m_globalPresence->currentPresence().statusMessage());

    m_globalPresence->setPresence(p);

    //Q_EMIT presenceChanged();
}

void PresenceManager::onGenericOperationFinished(Tp::PendingOperation *op)
{
    if (op->isError()) {
        kDebug() << op->errorName();
        kDebug() << op->errorMessage();
    }
}

void PresenceManager::onPresenceChanged(KTp::Presence)
{
    Q_EMIT presenceChanged();
}

void PresenceManager::onConnectionStatusChanged(Tp::ConnectionStatus) 
{
    // TODO: force busy settings icon 
}

#include "presence-manager.moc"
